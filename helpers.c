#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <jack/jack.h>
#include <jack/midiport.h>
#include "helpers.h"

jack_port_t *input_port;
jack_port_t *output_port1;
jack_port_t *output_port2;
jack_port_t *mic_port;
jack_client_t *client;

midi_event_callback_t midi_event_callback;
sample_callback_t sample_callback;

float last_value1 = 0;
float last_value2 = 0;

int process(jack_nframes_t nframes, void *arg) {
  //printf("n frames %d\n", nframes);
	int i;
	void* port_buf = jack_port_get_buffer(input_port, nframes);
	jack_default_audio_sample_t *out1 = (jack_default_audio_sample_t *) jack_port_get_buffer (output_port1, nframes);
	jack_default_audio_sample_t *out2 = (jack_default_audio_sample_t *) jack_port_get_buffer (output_port2, nframes);
	jack_default_audio_sample_t *mic_in = (jack_default_audio_sample_t *) jack_port_get_buffer (mic_port, nframes);
	jack_midi_event_t in_event;
	jack_nframes_t event_index = 0;
	jack_nframes_t event_count = jack_midi_get_event_count(port_buf);
	//if(event_count > 1)
	//{
	//	//printf(" midisine: have %d events\n", event_count);
	//	for(i=0; i<event_count; i++) {
  //    // get all events available imidiately
	//		jack_midi_event_get(&in_event, port_buf, i);
	//		printf("    event %d time is %d. 1st byte is 0x%x\n", i, in_event.time, *(in_event.buffer));
  //    midi_event_callback(in_event.buffer, in_event.size);
	//	}
	//}
	jack_midi_event_get(&in_event, port_buf, 0);
  float_pair_t val;
	for (i = 0; i < nframes; i++) {
		if (in_event.time == i && event_index < event_count) {
      // gets events as they become valid
      midi_event_callback(in_event.buffer, in_event.size);
			event_index++;
			if(event_index < event_count)
				jack_midi_event_get(&in_event, port_buf, event_index);
		}

    //if (i%2 == 0) {
    //  float_pair_t val = sample_callback(mic_in[i]);
    //  out1[i] = (last_value1 + val.a) / 2;
    //  out2[i] = (last_value2 + val.b) / 2;
    //  last_value1 = val.a;
    //  last_value2 = val.b;
    //} else {
    //  out1[i] = last_value1;
    //  out2[i] = last_value2;
    //}
    float_pair_t val = sample_callback(mic_in[i]);
    out1[i] = val.a;
    out2[i] = val.b;


	}
	return 0;      
}



int srate(jack_nframes_t nframes, void *arg) {
  printf("uh oh sample rate changed %d\n", nframes);
}

void jack_shutdown(void *arg)
{
	exit(1);
}

int start_jack(midi_event_callback_t midi_event_callback_,
    sample_callback_t sample_callback_) {

  midi_event_callback =midi_event_callback_;
  sample_callback =  sample_callback_;

	if ((client = jack_client_open ("midisine", JackNullOption, NULL)) == 0) {
		fprintf(stderr, "jack server not running?\n");
		return 1;
	}
	
	jack_set_process_callback (client, process, 0);
	jack_set_sample_rate_callback (client, srate, 0);
	jack_on_shutdown (client, jack_shutdown, 0);
	input_port = jack_port_register (client, "midi_in", JACK_DEFAULT_MIDI_TYPE, JackPortIsInput, 0);
	mic_port = jack_port_register (client, "mic_in", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
	output_port1 = jack_port_register (client, "audio_out1", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
	output_port2 = jack_port_register (client, "audio_out2", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);

	if (jack_activate (client)) {
		fprintf(stderr, "cannot activate client");
		return 1;
	}

  int err = jack_connect(client, "system:midi_capture_2", jack_port_name(input_port));
  //int err = jack_connect(client, "CASIO USB-MIDI:CASIO USB-MIDI MIDI 1 (out)", jack_port_name(input_port));


  int err2 = jack_connect(client, jack_port_name(output_port1), "system:playback_1");
  int err4 = jack_connect(client, jack_port_name(output_port2), "system:playback_2");
  int err3 = jack_connect(client, "system:midi_capture_1", jack_port_name(input_port));
  int err5 = 69;//jack_connect(client, "system:capture_1", jack_port_name(mic_port));
  //int err5 = jack_connect(client, "mpv:out_0", jack_port_name(mic_port));
  printf("connect errrores midi1: %d, speakers: %d %d, midi2: %d, mic: %d\n", err, err2, err3, err4, err5);
  printf("EEXIST = %d\n", EEXIST);
}

void close_jack() {
	jack_client_close(client);
}


