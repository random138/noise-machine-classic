typedef struct {
  float a;
  float b;
} float_pair_t;

typedef void (*midi_event_callback_t)(unsigned char *, int);
typedef float_pair_t (*sample_callback_t)(float);
int start_jack(midi_event_callback_t midi_event_callback_,
    sample_callback_t sample_callback_);
void close_jack();


