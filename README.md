setup:
pulseaudio --kill
qjackctl
OR /usr/bin/jackd -dalsa -dhw:0 -r44100 -p1024 -n2 -Xseq -D -Phw:Generic_1,0


You'll need this to compile:
sudo apt-get install libjack-dev 


compileing:
#gcc -o out source.c -lm `pkg-config --cflags --libs jack`
gcc -Ofast -o out helpers.c main.c -lm `pkg-config --cflags --libs jack`

monitor programs:
japa -J
x42-scope

