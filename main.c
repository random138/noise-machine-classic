// c
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <jack/jack.h>
#include <jack/midiport.h>
#include "helpers.h"

int is_on = 0;
unsigned int t = 0;



#define n_cords (12)
int cord_size = 64 + 1;
int cord_sizes[n_cords];
int measure_spots1[n_cords];
int measure_spots2[n_cords];
int hit_spots_meta[n_cords];
float hit_power_left[n_cords];
float hit_max_delta[n_cords];
//float dampening[n_cords];

float* piano;
float* piano_next;

float magic[8];

const float a_base = 0.0112;
float a_big = 0;
float b_small = 0;
float a = a_base;

const float b_base = 0.13;
float b_big = 0;
float a_small = 0;
float b = b_base;

const float c_base = 0.0;
float c_small = 0;
float c_big = 0.9;
float c = 0.9;

float d = 0.1;
int is_fixed = 1;

#define N_SCALERS 3
int bank = 0;
float random_buffers[4][6][8];
float rando_scales[8];
float rando_base[8];
float rando_small[8] = {1, 1, 1, 1, 1, 1, 1, 1};
int rando_scaler[8] = {1, 1, 1, 1, 1, 1, 1, 1};
const float rando_scaler_meaning[N_SCALERS] = {10.0, 0.1, 0.0};

int mic_enabled = 1;


double clamp(double d, double min, double max) {
  const double t = d < min ? min : d;
  return t > max ? max : t;
}


float* hold_spots[n_cords*4]; // right[2], middle[2]
float* measure_spots[n_cords*2]; // spots 1 and 2
float* hit_spots[n_cords];
void calculate_special_spots() {
  for (int i = 0; i < n_cords; i++) {
      int endy = cord_sizes[i];
      int start = 2*i*cord_size; // hold, +1
      int right_end = start + 2*cord_size-2; //hold
      int measure_spot1 = start + 2*measure_spots1[i];
      int measure_spot2 = start + 2*measure_spots2[i];
      int hit_spot = start + 2*hit_spots_meta[i];
      int middle_hold = start + endy*2;
      hold_spots[4*i] = piano + right_end;
      hold_spots[4*i+1] = piano + right_end + 1;
      hold_spots[4*i+2] = piano + middle_hold;
      hold_spots[4*i+3] = piano + middle_hold + 1;
      measure_spots[2*i] = piano + measure_spot1;
      measure_spots[2*i+1] = piano + measure_spot2;
      hit_spots[i] = piano + hit_spot;
      printf("i, hs: %d %d\n", i, hit_spot);
  }
}



void calculate_magic() {
  magic[0] = b;
  magic[1] = 0;
  magic[2] = 1;
  magic[3] = 0;
  magic[4] = 1-2*b;
  magic[5] = b-a;
  magic[6] = -2;
  magic[7] = 1;//d;

  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 8; j++) {
      magic[j] += (rando_scaler_meaning[rando_scaler[j]]) * (rando_small[j] * (rando_scales[j]+rando_base[j])) * random_buffers[bank][i][j];
    }
  }
}

void print_midi(unsigned char* buffer, int size) {
  for (int i = 0; i < size; i++) {
    printf("%d|", buffer[i]);
  }
  printf("midi\n");
}

void set_string_spots(int i, int bar) {
  // puts the hold spots and everythign in all the right places for a string
  // i'th string, at position bar
  cord_sizes[i] = bar;
  measure_spots1[i] = bar/3-1;
  measure_spots2[i] = 2*bar/3-1;
  hit_spots_meta[i] = bar/2+1;
  printf("i, bar, hsmeta: %d %d %d\n", i, bar, hit_spots_meta[i]);
}

//float last_value_x = 0;
//float last_value_y = 0;
void my_process_midi(unsigned char* buffer, int size) {

  if (size == 3 && buffer[0] == 144) {
    is_on = 1;
    int pos = buffer[1]%n_cords;
    //printf("pressed %d\n", buffer[1]);
    hit_power_left[pos] += 10*(hit_max_delta[pos] * 20) * ((float) buffer[2]) / 128;
    //b = pow(2.0, (buffer[1] - 93)/12.0);
    //printf("hit %f with = %f\n", b);
    calculate_magic();
    //dampening[pos] = 1;
  } else if(buffer[0] == 128) {
    int pos = buffer[1]%n_cords;
    is_on = 0;
    //dampening[pos] = 0.9;
    //printf("unpressed\n");
  } else if (buffer[0] == 176) {
    if (buffer[1] == 3) {
      a_big = 0.1 * (((float) buffer[2]) / 128 - 0.5);
      a = a_base + a_big + a_small;
      calculate_magic();
      //printf("a = %f\n", a);
    } else if (buffer[1] == 14) {
      a_small = 5*(0.1/128) * (((float) buffer[2]) / 128 - 0.5);
      a = a_base + a_big + a_small;
      calculate_magic();
      //////printf("a = %f\n", a);
    } else if (buffer[1] == 4) {
      // b
      b_big =  2.0 * (((float) buffer[2]) / 128 ) - 1;
      b = b_base + b_big + b_small;
      calculate_magic(a, b);
      //set_string_spots(0, buffer[2]);
      //calculate_special_spots();

    } else if (buffer[1] == 15) {
      b_small = 5*(1.0/128) * (((float) buffer[2]) / 128 );
      b = b_base + b_big + b_small;
      calculate_magic();
    } else if (buffer[1] == 5) {
      // c
      c_big =  (((float) buffer[2]) / 128);
      c = c_base + c_big + c_small;
      calculate_magic();
    } else if (buffer[1] == 16) {
      //c_small = 5*(1.0/128) * (((float) buffer[2]) / 128 - 0.5);
      //c = c_base + c_big + c_small;
      //d = pow(1.002, ((buffer[2] + 1) / 128.0)) - .002;
      d = buffer[2] / 128.0;
      calculate_magic();
    } else if (buffer[1] >= 6 && buffer[1] <= 11) { // sliders
      int pos = buffer[1] - 6;
      rando_scales[pos] = 0.1 *(((float) buffer[2]) / 128 - 0.5);
      calculate_magic();
    } else if (buffer[1] >= 17 && buffer[1] <= 22) { // knobs
      int pos = buffer[1] - 17;
      //rando_small[pos] = 5*(1.0/128) * 0.1 *(((float) buffer[2]) / 128 - 0.5);
      float foo =  10 * (((float) buffer[2]) / 128 - 0.5);
      rando_small[pos] = pow(10, foo);
      calculate_magic();
    } else if (buffer[1] >= 26 && buffer[1] <= 31 && buffer[2] == 127) { // the buttons
      int pos = buffer[1] - 26;
      //rando_scaler[pos] = (rando_scaler[pos] + 1) % N_SCALERS;
      rando_scaler[pos] = (rando_scaler[pos] + 1) % N_SCALERS;
      // rebase this one nah switch back to toggly do
      //rando_base[pos] = rando_scales[pos];
      //rando_scales[pos] = 0;
      for (int i = 0; i < pos; i++) {
        printf("\t"); // shift t positions
      }
      printf("%f\n", rando_scaler_meaning[rando_scaler[pos]]);
      calculate_magic();
    } else if (buffer[1] == 23 && buffer[2] == 127) { // button 1 silences piano (changed to disable mic)
      printf("disable or enable mic\n");
      mic_enabled = !mic_enabled;
      printf("mic enable = %d\n", mic_enabled);
      //for (int i = 0; i < 2*cord_size*n_cords; i++) {
        //piano[i] = 0;
      //}
      //last_value_x = 0;
      //last_value_y = 0;
    } else if (buffer[1] == 25 && buffer[2] == 127) { // 3rd button togges fixed points
      is_fixed = !is_fixed;
    } else if (buffer[1] == 24 && buffer[2] == 127) {
      for (int i = 0; i < 2*cord_size*n_cords; i++) {
        piano[i] = 1 * (((float) (rand() % 100)) / 100 - 0.5);
      }
      //last_value_x = 0;
      //last_value_y = 0;
    } else {
      //print_midi(buffer, size);
    }
  } else if (size == 11) {
    bank = buffer[9];
    printf("bank %d\n", bank);
    calculate_magic();
    // 250|66|64|0|1|4|0|95|79|2|247

  } else {
    print_midi(buffer, size);
  }
}


int first_time = 1;
int foo = 100;
float out_value_left_hist = 0;
float out_value_right_hist = 0;
float last_mic = 0;
float_pair_t my_sample_callback(float mic_in) {
  float x =  is_on ? ((t % foo < foo/2) ? 0.1 : -0.1) : 0;


  float out_value_left = 0;
  float out_value_right = 0;

  float** hold_temp = (float**) hold_spots;
  float** measure_temp = (float**) measure_spots;
  float** hit_temp = (float**) hit_spots;
  if (is_fixed) {
    piano[0] = 0;
    piano[1] = 0;
  }
  const float delta_mic = mic_enabled ? 10*clamp(1*(mic_in - last_mic), -.1, .1) : 0.0;
  for (int i = 0; i < n_cords; i++) {
    if (is_fixed) {
      **(hold_temp++) = 0;
      **(hold_temp++) = 0;
      **(hold_temp++) = 0;
      **(hold_temp++) = 0;
    }
    if (hit_max_delta[i] * hit_power_left[i] > 0) {
      hit_power_left[i] -= hit_max_delta[i];
      **(hit_temp) += 2*hit_max_delta[i];
    }
    *(*(hit_temp)+1) += 10*d*delta_mic;
    hit_temp++;
    //**(hit_temp++) = 100*d*mic_in;
    out_value_left += **(measure_temp++);
    out_value_right += **(measure_temp++);

  }
  last_mic = mic_in;

  float* in = piano;
  float* out = piano_next;
  for (int i = 0; i < n_cords * cord_size; i++) {
    float x = *(in++);
    float y = *(in++);
    *(out++) = x * magic[4] + y * magic[5];
    *(out++) = x * magic[6] + y * magic[7];
  }
  in = piano;
  float * out_l = piano_next -2;
  float * out_r = piano_next +2;
  for (int i = 0; i < n_cords * cord_size; i++) {
    float x = *(in++);
    float y = *(in++);
    float out_x = x * magic[0] + y * magic[1];
    float out_y = x * magic[2] + y * magic[3];
    *(out_l++) += out_x;
    *(out_l++) += out_y;
    *(out_r++) += out_x;
    *(out_r++) += out_y;
  }
  out = piano_next;
  for (int i = 0; i < 2*n_cords * cord_size; i++) {
    //out[i] = clamp(delta_mic+ out[i], -1, 1);
    out[i] = clamp(out[i], -1, 1);
  }
  float* swap_temp = piano;
  piano = piano_next;
  piano_next = swap_temp;

  t += 1;
  first_time = 0;

  float_pair_t ret;
  out_value_left_hist += (1-c) * (out_value_left - out_value_left_hist);
  out_value_right_hist += (1-c) * (out_value_right - out_value_right_hist);
  //last_value_x = 0;//0.01*out_value_x;
  ret.a = 0.3*out_value_left_hist;
  ret.b = 0.3*out_value_right_hist;
  return ret;
}


int main(int narg, char **args) {

  srand(31416);
  for (int banky = 0; banky < 4; banky++) {
    for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 8; j++) {
        if (j == 4 || j == 7) {
          continue;
        }
        //random_buffers[banky][i][j] = ((float) (rand() % 100)) / 100.0;
        random_buffers[banky][i][j] = rand() % 2 ? (rand() % 5) - 2 : 0;
      }
      rando_scales[i] = 0;
    }
  }


  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 8; j++) {
      random_buffers[0][i][j] = 0;
    }
  }
  random_buffers[0][0][1] = 1;
  random_buffers[0][1][2] = 1;
  random_buffers[0][2][3] = 1;
  random_buffers[0][3][4] = 1;
  random_buffers[0][4][5] = 1;
  random_buffers[0][5][6] = 1;

  calculate_magic();

  // each cord has a leading 2 floats to protect from eachother.
  // And the whole thing has 2 at the end which are written to 
  // to prevent writing outside the buffer on the end.
  piano = malloc(sizeof(float) * (2*n_cords * cord_size + 2));
  piano_next = malloc(sizeof(float) * (2*n_cords * cord_size + 2));
  piano = piano -2;
  piano_next = piano_next -2;

  float fooss[12] = {
    1.0/4, 
    2.0/4, 
    3.0/4, 
    4.0/4, 
    5.0/4, 
    1.0/3, 
    2.0/3,
    3.0/3,
    4.0/3,
    4.0/3,
    4.0/3,
    1};
  for (int i = 0; i < n_cords; i++) {
    //float foo = pow(2, ((float) (n_cords-i))/12 ); // from 0.5 to 2
    //float foo = pow(2, ((float) (n_cords-i))/12 ); // from 0.5 to 2
    float foo = fooss[i];
    int bar = (cord_size/2) * foo;
    set_string_spots(i, bar);
    //printf("oof %d, %f, %d\n", i, foo, bar);
    hit_power_left[i] = 0;
    hit_max_delta[i] = i % 2 == 0 ? 0.05 : -0.05;
    //dampening[i] = 1;
  }
  calculate_special_spots();


  start_jack(my_process_midi, my_sample_callback);
	while(1)
	{
		sleep(1);
	}
  close_jack();
	exit (0);

}

